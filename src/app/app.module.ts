import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {NavbarComponent} from './navbar/navbar.component';
import {HomeComponent} from './home/home.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {TableComponent} from './table/table.component';
import {HttpClientModule} from '@angular/common/http';
import {AddNewComponent} from './add-new/add-new.component';
import {RouterModule, Routes} from '@angular/router';
import {NgxPaginationModule} from 'ngx-pagination';
import {CategoryComponent} from './category/category.component';
import {ViewComponent} from './view/view.component';
import {UpdateBookComponent} from './update-book/update-book.component';

const routes: Routes = [
  {
    path: '', redirectTo: 'home', pathMatch: 'full',
  },
  {
    path: 'home',
    component: HomeComponent,
  }
  ,
  {
    path: 'addNew',
    component: AddNewComponent
  },
  {
    path: 'category',
    component: CategoryComponent
  },
  {
    path: 'view/:id',
    component: ViewComponent
  },
  {
    path: 'update/:id',
    component: UpdateBookComponent
  }
];

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    TableComponent,
    AddNewComponent,
    CategoryComponent,
    ViewComponent,
    UpdateBookComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
    FormsModule,
    NgxPaginationModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
