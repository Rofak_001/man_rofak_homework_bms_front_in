import {Component, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {ApiService} from '../api.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  books;
  isSearch = false;
  currentPage = 1;

  constructor(private apiService: ApiService) {
  }

  ngOnInit(): void {
  }

  onChange(s): void {
    if (s.control.value !== '') {
      this.apiService.searchBookByTitle(s.control.value).subscribe(res => {
        this.books = res._embedded.books;
        this.isSearch = true;
      });
    } else {
      this.isSearch = false;
      this.apiService.getBooks(this.currentPage - 1).subscribe(res => {
        this.books = res._embedded.books;
      });
    }
  }

  getFromChild(currentPage): void {
    this.currentPage = currentPage;
  }
}
