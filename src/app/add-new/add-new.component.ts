import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ApiService} from '../api.service';
// @ts-ignore
import placeholder from '../../assets/img/placeholder-image.png';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-add-new',
  templateUrl: './add-new.component.html',
  styleUrls: ['./add-new.component.css']
})
export class AddNewComponent implements OnInit {
  selectedFile;
  options;
  url = placeholder;

  constructor(private apiService: ApiService, private router: Router) {
  }

  ngOnInit(): void {
    this.apiService.getCategory().subscribe(data => {
      this.options = data._embedded.categories;
    });
  }

  onSubmit(f): void {
    let img = null;
    if (f.value.title !== '' && f.value.author !== '' && f.value.description !== '' && f.value.category) {
      if (this.selectedFile != null) {
        this.apiService.uploadImage(this.selectedFile).subscribe((res) => {
          img = res.fileDownloadUri;
          this.apiService.addBook({
            title: f.value.title,
            author: f.value.author,
            description: f.value.description,
            thumbnail: img,
            category:
              {
                id: f.value.category
              }
            // tslint:disable-next-line:no-shadowed-variable
          }).subscribe(res => {
            alert('Add Successfully!');
            this.router.navigate(['/home']);
          });
        });
      } else {
        alert('Please Select Image!');
      }

    } else {
      alert('Field Cannot empty!');
    }
  }

  onFileChanged(event): void {
    this.selectedFile = event.target.files[0];
    if (!event.target.files[0] || event.target.files[0].length === 0) {
      return;
    }
    const mimeType = event.target.files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }
    const reader = new FileReader();
    reader.readAsDataURL(event.target.files[0]);
    // tslint:disable-next-line:variable-name
    reader.onload = (_event) => {
      this.url = reader.result;
    };
  }
}

