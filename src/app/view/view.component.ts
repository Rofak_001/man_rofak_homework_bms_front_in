import {Component, OnInit} from '@angular/core';
// @ts-ignore
import placeholder from '../../assets/img/placeholder-image.png';
import {ApiService} from '../api.service';
import {ActivatedRoute} from '@angular/router';
import {Book} from '../model/Book';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {
  url = placeholder;
  title;
  author;
  description;
  category;

  constructor(private apiService: ApiService, private activeRoute: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.activeRoute.params.subscribe(param => {
      this.getBookById(param.id);
    });
  }

  getBookById(id): void {
    this.apiService.getBookById(id).subscribe(res => {
      this.url = res.thumbnail;
      this.title = res.title;
      this.author = res.author;
      this.description = res.description;
      this.category = res.category.title;
    });
  }
}
