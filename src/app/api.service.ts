import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private URL = 'http://localhost:8080/';

  constructor(private httpClient: HttpClient) {
  }

  public getBooks(page): Observable<any> {
    return this.httpClient.get(this.URL + 'books?page=' + page);
  }

  public getCategory(): Observable<any> {
    return this.httpClient.get(this.URL + 'categories');
  }

  public uploadImage(image: File): Observable<any> {
    const formData = new FormData();
    formData.append('file', image);
    return this.httpClient.post(this.URL + 'upload', formData);
  }

  public addBook(book): Observable<any> {
    return this.httpClient.post(this.URL + 'books', book);
  }

  public addCategory(category): Observable<any> {
    return this.httpClient.post(this.URL + 'categories', category);
  }

  public deleteCategory(id): Observable<any> {
    return this.httpClient.delete(this.URL + 'categories/' + id);
  }

  public updateCategory(id, category): Observable<any> {
    return this.httpClient.put(this.URL + 'categories/' + id, category);
  }

  public getCategoryById(id): Observable<any> {
    return this.httpClient.get(this.URL + 'categories/' + id);
  }

  public searchBookByTitle(title): Observable<any> {
    return this.httpClient.get(this.URL + 'books/search/bookTitle?title=' + title);
  }

  public getBookById(id): Observable<any> {
    return this.httpClient.get(this.URL + 'books/' + id);
  }

  public updateBook(id, book): Observable<any> {
    return this.httpClient.put(this.URL + 'books/' + id, book);
  }

  public deleteBook(id): Observable<any> {
    return this.httpClient.delete(this.URL + 'books/' + id);
  }
}
