import {Category} from './Category';

export interface Book {
  title: string;
  author: string;
  description: string;
  thumbnail: string;
  category: Category;
}
