import {Component, OnInit} from '@angular/core';
// @ts-ignore
import placeholder from '../../assets/img/placeholder-image.png';
import {ApiService} from '../api.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Book} from '../model/Book';

@Component({
  selector: 'app-update-book',
  templateUrl: './update-book.component.html',
  styleUrls: ['./update-book.component.css']
})
export class UpdateBookComponent implements OnInit {
  selectedFile;
  url = placeholder;
  book: Book;
  options;
  id;

  constructor(private apiService: ApiService, private activeRoute: ActivatedRoute, private router: Router) {
  }

  ngOnInit(): void {
    this.activeRoute.params.subscribe(param => {
      this.id = param.id;
      this.apiService.getBookById(param.id).subscribe(res => {
        this.book = res;
        this.url = res.thumbnail;
      });
    });
    this.apiService.getCategory().subscribe(data => {
      this.options = data._embedded.categories;
    });
  }

  onSubmit(f): void {
    let img = null;
    if (f.value.title !== '' && f.value.author !== '' && f.value.description !== '' && f.value.category) {
      if (this.selectedFile != null) {
        this.apiService.uploadImage(this.selectedFile).subscribe((res) => {
          img = res.fileDownloadUri;
          this.apiService.updateBook(this.id, {
            title: f.value.title,
            author: f.value.author,
            description: f.value.description,
            thumbnail: img,
            category:
              {
                id: f.value.category
              }
            // tslint:disable-next-line:no-shadowed-variable
          }).subscribe(res => {
            alert('Updated Successfully!');
            this.router.navigate(['/home']);
          });
        });
      } else {
        this.apiService.updateBook(this.id, {
          title: f.value.title,
          author: f.value.author,
          description: f.value.description,
          thumbnail: this.url,
          category:
            {
              id: f.value.category
            }
        }).subscribe(res => {
          alert('Updated Successfully!');
          this.router.navigate(['/home']);
        });
      }

    } else {
      alert('Field Cannot empty!');
    }
  }

  onFileChanged(event): void {
    this.selectedFile = event.target.files[0];
    if (!event.target.files[0] || event.target.files[0].length === 0) {
      return;
    }
    const mimeType = event.target.files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }
    const reader = new FileReader();
    reader.readAsDataURL(event.target.files[0]);
    // tslint:disable-next-line:variable-name
    reader.onload = (_event) => {
      this.url = reader.result;
    };
  }
}
