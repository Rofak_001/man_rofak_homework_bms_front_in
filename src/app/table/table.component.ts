import {Component, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {ApiService} from '../api.service';
import {EventEmitter} from '@angular/core';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {
  @Input() books;
  @Input() isSearch = false;
  @Output() valueChange = new EventEmitter();
  page;
  currentPage = 1;
  size = 0;
  totalItem = 0;

  constructor(private apiService: ApiService) {
  }

  changeValue(): void {
    this.valueChange.emit(this.currentPage);
  }

  ngOnInit(): void {
    this.apiService.getBooks(this.currentPage - 1).subscribe(data => {
      this.books = data._embedded.books;
      this.size = data.page.size;
      this.totalItem = data.page.totalElements;
    });
  }

  onTableDataChange(event): void {
    this.currentPage = event;
    this.apiService.getBooks(this.currentPage - 1).subscribe(data => {
      this.books = data._embedded.books;
    });
    this.changeValue();
    console.log(event);
  }

  onDelete(id): void {
    this.apiService.deleteBook(id).subscribe(res => {
      this.apiService.getBooks(this.currentPage - 1).subscribe(data => {
        this.books = data._embedded.books;
      });
    });
  }
}
