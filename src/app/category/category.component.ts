import {Component, OnInit} from '@angular/core';
import {ApiService} from '../api.service';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {
  categories;
  title;
  isUpdate = false;
  updateId = 0;

  constructor(private apiService: ApiService) {
  }

  ngOnInit(): void {
    this.getCategory();
  }

  onSubmit(f): void {
    if (f.value.category !== '' || f.value.category != null || f.value.category !== 'undefined') {
      if (this.isUpdate) {
        this.apiService.updateCategory(this.updateId, {title: f.value.category}).subscribe(res => {
          this.getCategory();
          this.isUpdate = false;
          this.updateId = 0;
        });
      } else {
        this.apiService.addCategory({title: f.value.category}).subscribe((res) => {
          this.getCategory();
        });
      }
    }
  }

  getCategory(): void {
    this.apiService.getCategory().subscribe((res) => {
      this.categories = res._embedded.categories;
    });
  }

  onDelete(id): void {
    this.apiService.deleteCategory(id).subscribe((res) => {
      this.getCategory();
    });
  }

  onClickUpdate(id): void {
    this.isUpdate = true;
    this.apiService.getCategoryById(id).subscribe(res => {
      this.title = res.title;
      this.updateId = res.id;
    });
  }
}
